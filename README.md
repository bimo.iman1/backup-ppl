# PoiPoLe-DTB Beasiswa Miskin dan Disabilitas

## Coverage
[![coverage report](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/ppl-c/dtb-beasiswa-miskin-dan-disabilitas/poipole-dtb-beasiswa-miskin-dan-disabilitas/badges/staging/coverage.svg?job=UnitTest)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/ppl-c/dtb-beasiswa-miskin-dan-disabilitas/poipole-dtb-beasiswa-miskin-dan-disabilitas/badges/staging/coverage.svg?job=UnitTest)

## Pipeline
[![pipeline report](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/ppl-c/dtb-beasiswa-miskin-dan-disabilitas/poipole-dtb-beasiswa-miskin-dan-disabilitas/badges/staging/pipeline.svg?job=UnitTest)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2020/ppl-c/dtb-beasiswa-miskin-dan-disabilitas/poipole-dtb-beasiswa-miskin-dan-disabilitas/badges/staging/pipeline.svg?job=UnitTest)

# PPLapangan Tembak & PoiPoLe - DTB Layanan Siswa Disabilitas (Back End) - bisaGo

>bisaGo is a mobile application made with Flutter and Django REST API to gather information from users about disability friendly facilities. 

## Developers

PPLapanganTembak
1. [Adzkia Aisyah Afrah Hardian](https://gitlab.cs.ui.ac.id/adzkia.aisyah)
2. [Agnes Handoko](https://gitlab.cs.ui.ac.id/agneshandoko)
3. [Fakhira Devina](https://gitlab.cs.ui.ac.id/hiradevina)
4. [Faza Siti Sabira Prakoso](https://gitlab.cs.ui.ac.id/fazasabirappl)
5. [Firriyal bin Yahya](https://gitlab.cs.ui.ac.id/feriyalbinyahya)

PoiPoLeGan
1. [Bimo Iman Smartadi](https://gitlab.cs.ui.ac.id/bimo.iman)
2. [Bayukanta Iqbal Gunawan](https://gitlab.cs.ui.ac.id/Bayukanta)
3. [Dzaky Noor Hasyim](https://gitlab.cs.ui.ac.id/NoorHasyim)
4. [Muhammad Abdurrahman](https://gitlab.cs.ui.ac.id/muhammad.abdurrahman71)
5. [Usman Sidiq](https://gitlab.cs.ui.ac.id/usman.sidiq71)



## Table of Content

* Install
* API Testing

## Install
The back end side uses django, for installing please use this command:

```bash
pip install -r requirements.txt
```

To check the dependencies that have been installed within file requirements, use this command:
```bash
pip list
```

## API
### 1. To register your account:
Make `POST` request to API endpoint `/api/register` with `name`, `phone_number`, `email`, `password` key.

### 2. To receive token auth
Make `POST` request to API endpoint `/api-token-auth/` with `username`, `password`, key. where the username is your `email`. <br>
It will return a json with key `token`.

### 3. To check your user detail:
Make `GET` request to API endpoint `/api/user-detail/` with `email` key. <br>
Alternatively, you can access `/api/user-detail/*email*` where the email is your email and without the asterisk.<br>
It will return a json with key:
* `username` : username that you can use to request your token. The same as your email
* `name` : Your full name. Saved in the last_name.
* `email` : Your email
* `phone_number` : Your phone number

### 4. To register a location:
Make `POST` request to API endpoint `/informasi-lokasi/lokasi/add/` with the following key:
* `name` : location name
* `altitude` : location altitude, a float.
* `longitude` : location longitude, a float.
* `alamat` : address
* `no_telp` : Phone number 
* `image` : string of sourc image `.jpg`

### 5. To view a location list:
Make `GET` request to API endpoint `/informasi-lokasi/lokasi/list` with the key `name`. <br>
It will return a json list of locations with key:
* `id`	: id of location
* `name` : location name
* `altitude` : location altitude, a float.
* `longitude` : location longitude, a float.
* `alamat` : address
* `no_telp` : Phone number 
* `image` : string of sourc image `.jpg`

### 6. To view a location details:
Make `GET` request to API endpoint `/informasi-lokasi/lokasi/detail/*nama_lokasi*` with the key `name`. <br>
It will return a json with key:
* `name` : location name
* `altitude` : location altitude, a float.
* `longitude` : location longitude, a float.
* `alamat` : address
* `no_telp` : Phone number 
* `image` : string of source image `.jpg`

### 7. To register a facility:
Make `POST` request to API endpoint `/informasi-lokasi/lokasi/add-fasilitas/*nama-lokasi*` with the following key:
* `tag` : Fasility Tag
    * The options are :
        * KURSI_RODA = 'KR'
        * LIFT_DISABILITAS = 'LF'
        * TOILET_DISABILITTAS = 'TD'
        * MASJID_MUSHOLA = 'MM'
        * GUIDING_BLOCK = 'GB'
        * BIDANG_MIRING = 'BM'
        * TEMAN_DISABILITAS = 'CP'
        * JURU_BAHASA_ISYARAT = 'JI'
        * TONGKAT_DISABILITAS_NETRA = 'TN'
        * KURSI_UMUM_DISABILITAS = 'KD'
        * TEMPAT_PARKIR_DISABILITAS = 'PK'
        * RUNNING_TEXT = 'RT'
		* TEMPAT_PARKIR_BIASA = 'TB'
<br>

* `deskripsi` : Facility description.
<br>
<b>Make sure you have token it your request header.</b>
<b>Add this to your request header. `Authorization` with value `token *your_token_from_request*`</b>

### 8. To see a facility list:
Make `GET` request to API endpoint `/informasi-lokasi/lokasi/list-fasilitas/*nama-lokasi*` with `name` key. <br>
It will return a json with the following key:
* `id`: id of the facility
* `nama_lokasi`: location name of the facility
* `deskripsi`: the description
* `creator`: User's last name who registered the facility
* `date_time`: Date when the facility was made
* `like`: how many people like this facility. default is 0
* `dislike`: how many people dislike this facility. default is 0
* `rating`: rating of the facility. Default is 3
* `tag`: Facility tag
* `image`: Image for facility
* `is_verified`: Verified status


### 9. To see a facility details:
Make `GET` request to API endpoint `/informasi-lokasi/lokasi/detail-fasilitas/*nama-lokasi*/id-fasilitas` with `name` key. <br>
It will return a json with the following key:
* `nama_lokasi`: location name of the facility
* `deskripsi`: the description
* `creator`: User's last name who registered the facility
* `date_time`: Date when the facility was made
* `like`: how many people like this facility. default is 0
* `dislike`: how many people dislike this facility. default is 0
* `rating`: rating of the facility. Default is 3
* `tag`: Facility tag
* `image`: Image for facility
* `is_verified`: Verified status


