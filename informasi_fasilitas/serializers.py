from rest_framework import serializers

from .models import Lokasi

class LokasiSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Lokasi
        fields = ('id', 'name', 'latitude', 'longitude', 'alamat', 'no_telp', 'image')