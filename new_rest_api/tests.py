# Create your tests here.
from rest_framework.test import APIRequestFactory, APITestCase, URLPatternsTestCase
from rest_framework import status
from django.test import TestCase, SimpleTestCase, Client
from django.urls import include, path, reverse
from registrasi.models import BisaGoUser
import json

class UserTests(APITestCase):
    urlpatterns = [
        path('api/', include('new_rest_api.urls')),
    ]

    def setUp(self):
        url = reverse('create-user')
        data = {'name': 'Astraykai', 
                'email':'astraykai@gmail.com',
                'phone_number':'089892218567',
                'password':'chingchenghanji',}
        self.client.post(url, data)


    def test_create_user(self):
        """
        Ensure we can create a new account object.
        """
        url = reverse('create-user')
        data = {'name': 'Astray', 
                'email':'astrayyahoo@gmail.com',
                'phone_number':'08989221856',
                'password':'chingchenghanji',}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(BisaGoUser.objects.count(), 2)
        user = BisaGoUser.objects.get(phone_number='08989221856').user
        self.assertEqual(user.last_name, 'Astray')
        
        url = reverse('user-details', kwargs={'email':'astrayyahoo@gmail.com'})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK) 
        json_test = json.loads(response.content)
        self.assertEqual(len(json_test), 4) #JSON Attribute

    def test_account_details(self):
        url = reverse('user-details', kwargs={'email':'astraykai@gmail.com'})
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK) 
        json_test = json.loads(response.content)
        self.assertEqual(len(json_test), 4) #JSON Attribute

    def test_account_list(self):
        url = reverse('user-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        json_test = json.loads(response.content)
        self.assertEqual(len(json_test), 1)
        
    def test_account_login(self):
        pass



class InfoTests(APITestCase, URLPatternsTestCase):
    pass