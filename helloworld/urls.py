from django.conf.urls import url
from django.urls import path
from .views import helloworld
from . import views

app_name = "helloworld"

urlpatterns = [
    path('', views.helloworld, name='helloworld'),
]