from django.apps import AppConfig


class NewRestApiConfig(AppConfig):
    name = 'new_rest_api'
