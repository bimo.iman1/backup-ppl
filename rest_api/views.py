from django.shortcuts import render
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from .serializers import UserSerializer, GroupSerializer
from allauth.socialaccount.providers.oauth2.client import OAuth2Client
from rest_auth.registration.serializers import SocialLoginSerializer
from rest_auth.registration.views import SocialLoginView
from .adapters import GoogleOAuth2AdapterIdToken
from registrasi.models import User as localuser
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes

from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.hashers import make_password, check_password
#####
@csrf_exempt
def user_list(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        snippets = localuser.objects.all()
        serializer = UserSerializer(snippets, many=True)
        return JsonResponse(serializer.data, safe=False)

    elif request.method == 'POST':
        name = request.POST['name']
        phone_number = request.POST['phone_number']
        password = make_password(request.POST['password'])
        email = request.POST['email']
        data = {'name' : name, 'phone_number' : phone_number, 'password' : password, 'email' : email}
        serializer = UserSerializer(data=data)
        if serializer.is_valid():
            create_account(email, request.POST['password'], email)
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)

def create_account(username, password, email):
    account = User.objects.create_user(username=username, password=password, email=email)
    account.save()


@csrf_exempt
def validate(request):
    #password = make_password(request.POST['password'])
    if request.method == 'POST':
        email = request.POST['email']
        user_to_validate = User.objects.get(email = email)
        if check_password(request.POST['password'],user_to_validate.password):
            return JsonResponse({'response' : 'GOOD'}, status=200)
        else:
            return JsonResponse({'response' : 'password not match'}, status=400)
    else:
        return JsonResponse({'response' : 'try post request instead'}, status=400)
####


# class UserViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows users to be viewed or edited.
#     """
#     queryset = User.objects.all().order_by('-date_joined')
#     serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class GoogleLoginView(SocialLoginView):
    adapter_class = GoogleOAuth2AdapterIdToken
    callback_url = "http://localhost:8000/api/v1/users/login/google/callback/"
    client_class = OAuth2Client
    serializer_class = SocialLoginSerializer

# Create your views here.
