from django.contrib import admin
from django.urls import path,include
from rest_framework import routers
from rest_framework.authtoken import views
import new_rest_api.views
from django.views.generic import TemplateView


urlpatterns = [
    path('user-list/', new_rest_api.views.user_list,name='user-list'),
    path('user-detail/<str:email>', new_rest_api.views.user_details,name='user-details'),
    path('user-detail/?email=<str:email>', new_rest_api.views.user_details,name='user-details-get'),
    path('register/', new_rest_api.views.register_user,name='create-user'),
]