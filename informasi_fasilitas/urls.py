from django.urls import path, include
from rest_framework import routers
from . import views

urlpatterns = [
    path('lokasi/list/', views.lokasi_list,name='lokasi-list'),
    path('lokasi/detail/<str:name>', views.lokasi_details,name='lokasi-details'),
    path('lokasi/add/', views.add_lokasi,name='add-lokasi'),
    path('lokasi/add-fasilitas/<str:nama_lokasi>', views.add_fasilitas, name='add-fasilitas'),
    path('lokasi/list-fasilitas/<str:nama_lokasi>', views.list_fasilitas, name='list-fasilitas'),
    path('lokasi/detail-fasilitas/<str:nama_lokasi>/<int:id>', views.detail_fasilitas, name='detail-fasilitas'),
]