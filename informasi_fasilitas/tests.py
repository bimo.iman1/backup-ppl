from django.test import TestCase
from django.db.utils import IntegrityError
from .models import Lokasi, Fasilitas, Komentar, KURSI_RODA, RUNNING_TEXT
from registrasi.models import BisaGoUser
from django.contrib.auth.models import User
import tempfile

# Create your tests here.
class InformasiFasilitasTest(TestCase):
    not_null_constraint_failed_message = 'NOT NULL constraint failed'

    def test_models_lokasi_not_created(self):
        with self.assertRaises(IntegrityError) as cm:
            obj = Lokasi(name=None)
            obj.save()
        self.assertTrue(str(cm.exception).startswith(self.not_null_constraint_failed_message))

    def test_models_create_new_lokasi(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        Lokasi.objects.create(
            name='Mall', 
            latitude=0.0, 
            longitude=0.0, 
            alamat='Jl. Raya Bogor no.1, Jakarta', 
            no_telp='081212123131',
            image=image
        )
        count = Lokasi.objects.all().count()
        self.assertNotEqual(count, 0)

    def test_models_fasilitas_not_created(self):
        with self.assertRaises(IntegrityError) as cm:
            obj = Fasilitas(lokasi=None)
            obj.save()
        self.assertTrue(str(cm.exception).startswith(self.not_null_constraint_failed_message))
        
    def test_models_create_new_fasilitas(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        user=User(
            username='user@gmail.com',
            last_name='name',
            email='user@gmail.com', 
            password='hahagotim'
        )
        user.save()
        lokasi=Lokasi(
            name='Mall', 
            latitude=0.0, 
            longitude=0.0, 
            alamat='Jl. Raya Bogor no.1, Jakarta', 
            no_telp='081212123131',
            image=image
        )
        lokasi.save()
        Fasilitas.objects.create(
            lokasi=lokasi, 
            user=user, 
            deskripsi="penjelasan panjang", 
            like=0, 
            dislike=0, 
            rating=5, 
            tag={KURSI_RODA, RUNNING_TEXT}, 
            image=image
        )
        count = Fasilitas.objects.all().count()
        self.assertNotEqual(count, 0)

    def test_models_komentar_not_created(self):
        with self.assertRaises(IntegrityError) as cm:
            obj = Komentar(fasilitas=None)
            obj.save();
        self.assertTrue(str(cm.exception).startswith(self.not_null_constraint_failed_message))

    def test_models_create_new_komentar(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        user=User(
            username='user',
            last_name='name',
            email='user@gmail.com', 
            password='hahagotim'
        )
        user.save()
        lokasi=Lokasi(
            name='Mall', 
            latitude=0.0, 
            longitude=0.0, 
            alamat='Jl. Raya Bogor no.1, Jakarta', 
            no_telp='081212123131',
            image=image
        )
        lokasi.save()
        fasilitas=Fasilitas(
            lokasi=lokasi, 
            user=user, 
            deskripsi="penjelasan panjang", 
            like=0, 
            dislike=0, 
            rating=5, 
            tag={KURSI_RODA, RUNNING_TEXT}, 
            image=image
        )
        fasilitas.save()
        Komentar.objects.create(
            user=user,
            fasilitas=fasilitas,
            deskripsi="penjelasan panjang"
        )
        count = Komentar.objects.all().count()
        self.assertNotEqual(count, 0)

def test_view_lokasi(self):
    image = tempfile.NamedTemporaryFile(suffix=".jpg").name
    lokasi=Lokasi(
        name='Sekolah',
        latitude=0.0,
        longitude=0.0,
        alamat='Soppeng sentosa',
        no_telp='081212123131',
        image=image
    )
    lokasi.save()