from django.db import models
from django.contrib.auth.models import User
from multiselectfield import MultiSelectField

# Create your models here.
KURSI_RODA = 'KR'
LIFT_DISABILITAS = 'LF'
TOILET_DISABILITTAS = 'TD'
MASJID_MUSHOLA = 'MM'
GUIDING_BLOCK = 'GB'
BIDANG_MIRING = 'BM'
TEMAN_DISABILITAS = 'CP'
JURU_BAHASA_ISYARAT = 'JI'
TONGKAT_DISABILITAS_NETRA = 'TN'
KURSI_UMUM_DISABILITAS = 'KD'
TEMPAT_PARKIR_DISABILITAS = 'PK'
RUNNING_TEXT = 'RT'
TEMPAT_PARKIR_BIASA = 'TB'

FACILITIES = (
    (KURSI_RODA, 'Kursi Roda'),
    (LIFT_DISABILITAS, 'Lift Disabilitas'),
    (TOILET_DISABILITTAS, 'Toilet Disabilitas'),
    (MASJID_MUSHOLA, 'Masjid/Mushola'),
    (GUIDING_BLOCK, 'Guiding Block'),
    (BIDANG_MIRING, 'Bidang Miring'),
    (TEMAN_DISABILITAS, 'Teman Disabilitas'),
    (JURU_BAHASA_ISYARAT, 'Juru Bahasa Isyarat'),
    (TONGKAT_DISABILITAS_NETRA, 'Tongkat Disabilitas Netra'),
    (KURSI_UMUM_DISABILITAS, 'Kursi Umum Disabilitas'),
    (TEMPAT_PARKIR_DISABILITAS, 'Tempat Parkir Disabilitas'),
    (RUNNING_TEXT, 'Running Text'),
    (TEMPAT_PARKIR_BIASA, 'Tempat Parkir Biasa')
    )


class Lokasi(models.Model):
    name = models.CharField(max_length=50)
    latitude = models.FloatField()
    longitude = models.FloatField()
    alamat = models.CharField(max_length=100)
    no_telp = models.CharField(max_length=16)
    image = models.ImageField(upload_to="static/img", null=True)

    POINT_FIELD = ""+str(latitude)+","+str(longitude)

    def __str__(self):
        return self.name

class Fasilitas(models.Model):
    lokasi = models.ForeignKey(Lokasi, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date_time = models.DateTimeField(auto_now_add=True)
    deskripsi = models.TextField()
    like = models.IntegerField(default=0)
    dislike = models.IntegerField(default=0)
    rating = models.IntegerField(default=3)  
    tag = MultiSelectField(choices=FACILITIES, null=True, default=None)
    image = models.ImageField(upload_to="static/img", null=True, default=None)
    is_verified = models.BooleanField(default=False)

class Komentar(models.Model):
    fasilitas =models.ForeignKey(Fasilitas, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date_time = models.DateTimeField(auto_now_add=True)
    deskripsi = models.TextField()