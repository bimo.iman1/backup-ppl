from django.apps import AppConfig


class InformasiFasilitasConfig(AppConfig):
    name = 'informasi_fasilitas'
