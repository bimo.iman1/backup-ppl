# Generated by Django 2.1.5 on 2020-04-18 09:06

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import multiselectfield.db.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Fasilitas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_time', models.DateTimeField(auto_now_add=True)),
                ('deskripsi', models.TextField()),
                ('like', models.IntegerField(default=0)),
                ('dislike', models.IntegerField(default=0)),
                ('rating', models.IntegerField(default=3)),
                ('tag', multiselectfield.db.fields.MultiSelectField(choices=[('KR', 'Kursi Roda'), ('LF', 'Lift Disabilitas'), ('TD', 'Toilet Disabilitas'), ('MM', 'Masjid/Mushola'), ('GB', 'Guiding Block'), ('BM', 'Bidang Miring'), ('CP', 'Teman Disabilitas'), ('JI', 'Juru Bahasa Isyarat'), ('TN', 'Tongkat Disabilitas Netra'), ('KD', 'Kursi Umum Disabilitas'), ('PK', 'Tempat Parkir Disabilitas'), ('RT', 'Running Text'), ('TB', 'Tempat Parkir Biasa')], default=None, max_length=38, null=True)),
                ('image', models.ImageField(default=None, null=True, upload_to='static/img')),
                ('is_verified', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Komentar',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_time', models.DateTimeField(auto_now_add=True)),
                ('deskripsi', models.TextField()),
                ('fasilitas', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='informasi_fasilitas.Fasilitas')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Lokasi',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('latitude', models.FloatField()),
                ('longitude', models.FloatField()),
                ('alamat', models.CharField(max_length=100)),
                ('no_telp', models.CharField(max_length=16)),
                ('image', models.ImageField(null=True, upload_to='static/img')),
            ],
        ),
        migrations.AddField(
            model_name='fasilitas',
            name='lokasi',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='informasi_fasilitas.Lokasi'),
        ),
        migrations.AddField(
            model_name='fasilitas',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
