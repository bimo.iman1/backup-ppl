from django.db import models
from django.core.mail import send_mail
from django.contrib.auth.models import User


class BisaGoUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="phone_number")
    phone_number = models.CharField('Nomor Telepon',max_length=15, unique=True)
    
    def __str__(self):
        return self.user.username

    
