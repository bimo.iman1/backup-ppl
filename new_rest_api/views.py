from django.shortcuts import render
from django.contrib.auth.models import User, Group
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.hashers import make_password, check_password

from allauth.socialaccount.providers.oauth2.client import OAuth2Client

from rest_auth.registration.serializers import SocialLoginSerializer
from rest_auth.registration.views import SocialLoginView
from rest_framework import viewsets
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes, authentication_classes

from registrasi.models import BisaGoUser

def request_error_message(request_kind):
    return "get {} request instead".format(request_kind)

def missing_key_message(key):
    return "bad request. {} key needed".format(key)


@csrf_exempt
@api_view(['GET'])
@authentication_classes([])
@permission_classes([])
def user_list(request):
    if request.method == 'GET':
        user_list = BisaGoUser.objects.all()
        json_return = []
        for user in user_list:
            json_return.append({"username":user.user.email,
                       "name": user.user.last_name,
                       "email": user.user.email,
                       "phone_number": user.phone_number})
        return JsonResponse(json_return, safe=False)
    else:
        return JsonResponse({'response' : request_error_message("get")}, status = 400)

@api_view(['GET'])
@authentication_classes([])
@permission_classes([])
def user_details(request,email):
    if request.method == 'GET':
        user = User.objects.get(username = email)
        bisa_go_user = BisaGoUser.objects.get(user=user)
        json_return = {"username":user.email,
                       "name": user.last_name,
                       "email": user.email,
                       "phone_number": bisa_go_user.phone_number}
        return JsonResponse(json_return, safe=False)


@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def register_user(request):
    try:
        if request.method == 'POST':
            name = request.POST['name']
            phone_number = request.POST['phone_number']
            email = request.POST['email']
            password = request.POST['password']
            user = User.objects.create_user(username=email, email=email, password=password, last_name=name)
            BisaGoUser.objects.create(user= user, phone_number=phone_number)
            return JsonResponse({'response' : 'User created', 'email':email, 'name':name}, status = 201)
    except KeyError as e:
        return JsonResponse({'response':missing_key_message(str(e))}, status = 500)
            