from django.test import TestCase
from .models import BisaGoUser
from django.contrib.auth.models import User
from django.test import TestCase
from django.db.utils import IntegrityError
import tempfile


class RegistrationTest(TestCase):
    def test_user_model_created(self):
        user = User.objects.create(username="hoho", email="user@gmail.com", password="hohoho")
        BisaGoUser.objects.create(user=user, phone_number='081278787878')
        count = BisaGoUser.objects.all().count()
        self.assertNotEqual(count,0)

    def test_user_model_not_created(self):
        with self.assertRaises(IntegrityError) as cm:
            obj = BisaGoUser(user=None)
            obj.save();
        self.assertTrue(str(cm.exception).startswith('NOT NULL constraint failed'))