from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User

from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.authentication import TokenAuthentication 

from .serializers import LokasiSerializer
from .models import Lokasi, Fasilitas, Komentar

def request_error_message(request_kind):
    return "get {} request instead".format(request_kind)

def missing_key_message(key):
    return "bad request. {} key needed".format(key)

@csrf_exempt
@api_view(['GET'])
@authentication_classes([])
@permission_classes([])
def lokasi_list(request):
    if request.method == 'GET':
        lokasi_list = Lokasi.objects.all()
        serializer = LokasiSerializer(lokasi_list, many=True)
        return JsonResponse(serializer.data, safe=False)
    else:
        return JsonResponse({'response' : request_error_message("get")}, status = 400)

@api_view(['GET'])
@authentication_classes([])
@permission_classes([])
def lokasi_details(request,name):
    try:
        if request.method == 'GET':
            lokasi = Lokasi.objects.get(name = name)
            serializer = LokasiSerializer(lokasi)
            return JsonResponse(serializer.data, safe=False)
        else:
            return JsonResponse({'response' : request_error_message("get")}, status = 400)
    except KeyError as e:
        return JsonResponse({'response': missing_key_message(str(e))}, status = 500)
    except:
        return JsonResponse({'response':'lokasi not found'}, status = 404)

@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def add_lokasi(request):
    try:
        if request.method == 'POST':
            name = request.POST['name']
            latitude = request.POST['latitude']
            longitude = request.POST['longitude']
            alamat = request.POST['alamat']
            no_telp = request.POST['no_telp']
            image = request.POST['image']
            Lokasi.objects.create(name=name, latitude=latitude, longitude=longitude, alamat=alamat, no_telp=no_telp, image=image)
            return JsonResponse({'response' : 'Location added', 'name':name}, status = 201)
        else:
            return JsonResponse({'response' : request_error_message("post")}, status = 400)
    except KeyError as e:
        return JsonResponse({'response': missing_key_message(str(e))}, status = 500)

@api_view(['GET'])
@authentication_classes([TokenAuthentication,])
@permission_classes([])
def list_fasilitas(request, nama_lokasi):
    try:
        if request.method == 'GET':
            lokasi = Lokasi.objects.get(name = nama_lokasi)
            list_fasilitas = Fasilitas.objects.filter(lokasi=lokasi)
            return_json = {}
            for fasilitas in list_fasilitas:
                return_json[fasilitas.id] = {}
                fasilitas_details = return_json[fasilitas.id]
                fasilitas_details["id"] = fasilitas.id
                fasilitas_details["nama_lokasi"] = fasilitas.lokasi.name
                fasilitas_details["deskripsi"] = fasilitas.deskripsi
                fasilitas_details["creator"] = fasilitas.user.last_name
                fasilitas_details["date_time"] = fasilitas.date_time
                fasilitas_details["like"] = fasilitas.like
                fasilitas_details["dislike"] = fasilitas.dislike
                fasilitas_details["rating"] = fasilitas.rating
                fasilitas_details["tag"] = fasilitas.tag
                fasilitas_details["image"] = str(fasilitas.image)
                fasilitas_details["is_verified"] = fasilitas.is_verified
            return JsonResponse(return_json, status = 201)
    except KeyError as e:
        return JsonResponse({'response': missing_key_message(str(e))}, status = 500)
    except Exception as e:
        return JsonResponse({'response': str(e)})

@api_view(['POST'])
@authentication_classes([TokenAuthentication,])
@permission_classes([])
def add_fasilitas(request, nama_lokasi):
    try:
        if request.method == 'POST':
            lokasi = Lokasi.objects.get(name=nama_lokasi)
            user = User.objects.get(email=str(request.user))
            deskripsi = request.POST['deskripsi']
            image = request.POST['image']
            fasilitas = Fasilitas.objects.create(lokasi=lokasi,
                                     user=user,
                                     deskripsi=deskripsi,
                                     image=image)
            return JsonResponse({'response' : 'fasilitas added', 'id':fasilitas.id}, status = 201)
        else:
            return JsonResponse({'response' : request_error_message("post")}, status = 400)
    except KeyError as e:
        return JsonResponse({'response': missing_key_message(str(e))}, status = 500)
    except Exception as e:
        return JsonResponse({'response': str(e)}, status = 404)

@api_view(['GET'])
@authentication_classes([])
@permission_classes([])
def detail_fasilitas(request, nama_lokasi, id):
    try:
        if request.method == 'GET':
            lokasi = Lokasi.objects.get(name = nama_lokasi)
            fasilitas = Fasilitas.objects.get(lokasi=lokasi, id=id)
            user = fasilitas.user
            return_json = {"nama_lokasi": lokasi.name, "deskripsi":fasilitas.deskripsi, "creator":user.last_name, "date_time":fasilitas.date_time,
            "like":fasilitas.like, "dislike":fasilitas.dislike, "rating":fasilitas.rating, "tag":fasilitas.tag, "image":str(fasilitas.image), "is_verified":fasilitas.is_verified}
            return JsonResponse(return_json, status = 201)
        else:
            return JsonResponse({'response' : request_error_message("get")}, status = 400)
    except KeyError as e:
        return JsonResponse({'response': missing_key_message(str(e))}, status = 500)
    except Exception as e:
        return JsonResponse({'response': str(e)}, status = 404)