from django.contrib import admin
from .models import Lokasi, Fasilitas, Komentar

# Register your models here.
admin.site.register(Lokasi)
admin.site.register(Fasilitas)
admin.site.register(Komentar)
